    class Redis < FPM::Cookery::Recipe
      homepage 'http://redis.io'
      source   'https://download.redis.io/releases/redis-6.0.10.tar.gz'

      name     'redis-server'
      version  '6.0'
      revision '10'

      description 'An advanced key-value store.'

      conflicts 'redis-server'
      fpm_attributes rpm_os: 'linux'

      config_files '/etc/redis/redis.conf'


      def build
        make

        # Fix up default conf file to match our paths
        inline_replace "redis.conf" do |s|
          s.gsub! 'daemonize no', 'daemonize yes'
          s.gsub! 'logfile stdout', 'logfile /var/log/redis/redis-server.log'
          s.gsub! 'dir ./', 'dir /var/lib/redis/'
        end
      end

      def install
        %w(run lib/redis log/redis).each { |p| var(p).mkpath }

        bin.install Dir["src/redis-*"].select{ |f| f =~ /redis-[^\.]+$/ }

        etc('redis').install %w(redis.conf sentinel.conf)
        # etc('init.d').install 'redis-server.init.d' => 'redis-server'
      end
    end