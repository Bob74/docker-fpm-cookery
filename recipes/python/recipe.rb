    class Redis < FPM::Cookery::Recipe
      homepage 'http://redis.org'

      name     'python'
      version  '3.9.1'
      source   'https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tgz'

      description 'Python langage.'

      fpm_attributes rpm_os: 'linux'


      def build
        configure \
          '--enable-optimizations',
          '--enable-shared',
          '--prefix=/opt/python/python3.9.1'
        make
      end

      def install
        make :install, 'DESTDIR' => destdir
      end
    end