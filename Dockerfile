FROM alpine:3.13

RUN apk --no-cache --update add build-base rpm ruby ruby-dev ruby-etc gcc libffi-dev make libc-dev bash git curl tar unzip zlib-dev \
  && gem install fpm-cookery -v 0.35.1 && mkdir /ext

COPY . /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["fpm-cook"]
