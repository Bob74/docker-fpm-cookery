#!/bin/bash

# Signal fpm-cook that we are already running inside a container to avoid
# trying to start another one.
export FPMC_INSIDE_DOCKER=true

# Remove existing temporary directories to ensure a full build
cd recipes/$1
shift
fpm-cook "$@"
cp pkg/* /ext
rm -rf cache tmp-* pkg
